using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using Protractor;
using RecipeTestProject.Reporting;
using TestContext = NUnit.Framework.TestContext;

namespace RecipeTestProject
{
    [TestFixture]
    public class TestCases
    {

        IWebDriver driver = new ChromeDriver();


        WebDriverWait wait;
        [OneTimeSetUp]
        [Test, Order(1)]
        public void InitTests()
        {
            var appURL = TestContext.Parameters["webAppUrl"];
            driver.Navigate().GoToUrl(appURL);
            driver.Manage().Window.Maximize();

            driver = new NgWebDriver(driver);
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            ExtentReport.initReport();

        }

        [OneTimeTearDown]
        public void TearDown()
        {
            driver.Quit();
                        ExtentReport.EndReport();

        }

        // Browsing Recipes:
        // Test that when clicking on a recipe:
        // 1) Image is displayed - bonus points if you validate the image size or can compare to the src image (might have to get creative)
        // 2) Ensure recipe has a title, Description and ingredients

        [Test, Order(1)]
        public void ValidateRecipes()
        {
            ExtentReport.CreateScenarionNode("ValidateRecipes");
            try
            {
                
                driver.FindElement(By.XPath("//h4[text()='Butter Chicken']//..")).Click();
                ExtentReport.Pass("Click Recipe is successful");

                Thread.Sleep(2000);


                bool image = driver.FindElement(By.XPath("//img[@alt='Butter Chicken' and contains(@style,'300')]")).Displayed;
                bool title = driver.FindElement(By.XPath("//h1[text()='Butter Chicken']")).Displayed;
                IList<IWebElement> Ingredients = driver.FindElements(By.XPath("//ul[@class='list - group']//li"));

                if(image!=true && title!=true && Ingredients.Count>0)
                {
                    Assert.IsTrue(false, " Recipe validation faild"); 
                }

                ExtentReport.Pass("Passed Recipe Page validation"); 

            }
            catch (Exception e)

            {
                Assert.Fail(e.Message);
            }
        }


        // Managing Recipes:
        // Test for a particular recipe:
        // 1) That you can click the "Manage Recipes" button and that the 3 options are available/clickable
        // 2) That "Add Ingredients to Shopping List" does indeed add new items to the Shopping list menu.


        [Test,Order(2)]
        public void ManageRecipes()
        {
            ExtentReport.CreateScenarionNode("ManageRecipes");

            try
            {

                //getting initial count list for added recipe on the shopping list and moving back to the recipe screen
                driver.FindElement(By.XPath("//a[text()='Shopping List']")).Click();
                wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//label[text()='Name']")));
                int Init_AllAddedRecipe = driver.FindElements(By.XPath("//ul[@class='list-group']/a")).Count;
                Thread.Sleep(2000);
                driver.FindElement(By.XPath("//a[text()='Recipes']")).Click();


                //validating manage reciepts 
                driver.FindElement(By.XPath("//h4[text()='Butter Chicken']//..")).Click();
                Thread.Sleep(2000);

                ExtentReport.Pass("Clicked Reccipe Succesfully");

                wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.XPath("//button[text()=' Manage Recipe ']//..")));
                driver.FindElement(By.XPath("//button[@class='btn btn-primary dropdown-toggle']")).Click();
                Thread.Sleep(2000);

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
                wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[text()='Edit Recipe']")));
                wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[text()='Add Ingredients to Shopping List']")));
                wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[text()='Delete Recipe']")));

                ExtentReport.Pass("Validate Recipe Page");

                //adding recipe
                Thread.Sleep(3000);
                driver.FindElement(By.XPath("//a[text()='Add Ingredients to Shopping List']")).Click();
                int  RecipeToBeAdded = driver.FindElements(By.XPath("//ul[@class='list-group']/li")).Count;
                ExtentReport.Pass("Added Recipe");



                //validate if the recipe is added successful
                driver.FindElement(By.XPath("//a[text()='Shopping List']")).Click();
                wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//label[text()='Name']")));

                int  After_AllAddedRecipe = driver.FindElements(By.XPath("//ul[@class='list-group']/a")).Count;

                if(After_AllAddedRecipe != Init_AllAddedRecipe+ RecipeToBeAdded)
                {
                    ExtentReport.Fail("Recipe is not added successfully");
                    Assert.Fail("Recipe is not added successfully");
                    
                }
            }
            catch (Exception e)

            {
                ExtentReport.Fail(e.Message);
                Assert.Fail(e.Message);
            }
        }

        // Shopping List:
        // This will be a failing test (as the functionality is not yet in place, but:
        // 1) After adding Ingredients from the previous page, test to see if they are added alphabetically
        // 2) Add a new Ingredient using the Shopping List > Name and Amount fields
        // 3) Check again to see if this item has been added alphabetically

        // Any other additions you'd like to make that you think would add value.
        // Please structure your tests in a way that you believe would be easy to maintain and understand.
        // Refactor existing InitTests() method if you see anything you would do differently
        [Test, Order(3)]
        public void ShoppingList()
        {
            bool initsortOutcome = false, afterAddSortOutcome = false;
            ExtentReport.CreateScenarionNode("ShoppingList");

            try
            {
                driver.FindElement(By.XPath("//a[text()='Shopping List']")).Click();
                ExtentReport.Pass("Click Shopping list");

                wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//label[text()='Name']")));
                var Init_AllAddedRecipe = driver.FindElements(By.XPath("//ul[@class='list-group']/a"));
                var x = Init_AllAddedRecipe.Select(item => item.Text.Replace(System.Environment.NewLine, ""));

                //checking if items are sort initially
                var sorted = new List<string>();
                sorted.AddRange(x.OrderBy(o => o));
                if (x.SequenceEqual(sorted))
                {
                    initsortOutcome = true;
                }

                //adding item

                driver.FindElement(By.Id("name")).SendKeys("Pepper");
                ExtentReport.Pass("Enter Recipe Name");

                driver.FindElement(By.Id("amount")).SendKeys("100");
                ExtentReport.Pass("Enter Recipe amount");

                var after = Init_AllAddedRecipe.Select(item => item.Text.Replace(System.Environment.NewLine, ""));

                driver.FindElement(By.XPath("//button[text()='Add']")).Click();


                var sorted2 = new List<string>();
                sorted.AddRange(after.OrderBy(o => o));
                if (after.SequenceEqual(sorted))
                {
                    afterAddSortOutcome = true;
                }

                if (afterAddSortOutcome == false || initsortOutcome == false)
                {
                    ExtentReport.Fail("Iteams are not sorted");

                    Assert.Fail("Iteams are not sorted");
                }
            }
            catch (Exception e)

            {
                ExtentReport.Fail(e.Message);

                Assert.Fail(e.Message);
            }
        }


    }
}
