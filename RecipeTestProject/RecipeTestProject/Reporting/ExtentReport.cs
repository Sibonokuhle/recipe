﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeTestProject.Reporting
{
    public class ExtentReport
    {

        public static ExtentReports extent;
        public static ExtentHtmlReporter hmtlreport;
        private static ExtentTest TestFeature;
        private static ExtentTest scenario;

        public static void  initReport()
        {
            //To obtain the current solution path/project path

            string pth = System.Reflection.Assembly.GetCallingAssembly().CodeBase;

            string actualPath = pth.Substring(0, pth.LastIndexOf("bin"));

            string projectPath = new Uri(actualPath).LocalPath;
      
            //Append the html report file to current project path

            string reportPath = projectPath + "Reports\\TestRunReport.html"; 
            hmtlreport = new ExtentHtmlReporter(reportPath);


            //Boolean value for replacing exisisting report

            extent = new ExtentReports();
            extent.AttachReporter(hmtlreport);


            //Add QA system info to html report

            extent.AddSystemInfo("Host Name", "Local");
            extent.AddSystemInfo("Environment", "Recipe");

            TestFeature = extent.CreateTest("Recipe test");
        }

        public static void Fail (string reason)
        {
            scenario.Fail(reason);
        }
        public static void Pass(string reason)
        {
            scenario.Pass(reason);
        }
        public static void CreateScenarionNode(String s)
        {
            scenario = TestFeature.CreateNode(s);
        }
        public static void EndReport()
        {
            extent.Flush();
        }

    }
}
